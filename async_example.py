import asyncio

# async def my_corutine() -> None:
#     print("Hello world!")


async def corutine_add_one(number: int) -> int:
    return number + 1

# def add_one(number: int) -> int:
#     return number + 1

# function_result = add_one(1)
# corutine_result = corutine_add_one(1)

# print(f"func res: {function_result}")
# print(f"corutine res: {corutine_result}")

result = asyncio.run(corutine_add_one(1))
print(result)